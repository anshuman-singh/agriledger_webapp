import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardCoopsComponent } from './dashboard-coops.component';

describe('DashboardCoopsComponent', () => {
  let component: DashboardCoopsComponent;
  let fixture: ComponentFixture<DashboardCoopsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardCoopsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardCoopsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
